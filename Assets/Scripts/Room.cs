using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public Spawner[] spawners;
    public BaseEnemy[] possibleEnemies;
    public ItemBase[] possibleItems;
    public int maxEnemies;

    // Start is called before the first frame update
    void Start()
    {
        int enemiesAux = 0;

        if (maxEnemies >= spawners.Length)
            maxEnemies = spawners.Length / 2;

        int i = 0;

        while (i < spawners.Length)
        {
            var spawnId = Random.Range(0, spawners.Length);
            Spawner spawner = spawners[spawnId];
            if (!spawner.isOccuped)
            {
                spawner.isOccuped = true;

                if (enemiesAux < maxEnemies)
                {
                    BaseEnemy enemy = possibleEnemies[Random.Range(0, possibleEnemies.Length)];
                    Instantiate(enemy, spawner.transform.position, Quaternion.identity)
                        .startingPoint = spawner.transform;
                    enemiesAux++;
                }
                else
                {
                    float probability = Random.value;
                    ItemBase item = possibleItems[Random.Range(0, possibleItems.Length)];
                    Debug.Log(">>>"+probability);
                    if (probability > 0f && probability < 0.11)
                    {
                        Instantiate(item, spawner.transform.position, Quaternion.identity);
                    }
                }
                i++;
            }
        }
    }
}
