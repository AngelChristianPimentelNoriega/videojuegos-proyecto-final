using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEnemy : MonoBehaviour
{
    public float speed;
    public bool chase = false;
    public Transform startingPoint;
    public float attackCooldown;
    public bool canAttack = true;
    public LayerMask playerLayer;
    public int attackRange;
    public float health;

    protected GameObject player;
    protected float timeSinceAttack;

    public GameObject bloodEffect;


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        canAttack = true;
    }

    protected virtual void Chase()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }

    protected virtual void Flip()
    {
        if (player != null && transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    protected virtual void ReturnToStartingPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, startingPoint.position, (speed * 2) * Time.deltaTime);
    }

    protected virtual void VerifyCooldown()
    {
        if (!canAttack)
        {
            timeSinceAttack += Time.deltaTime;
        }

        if (timeSinceAttack >= attackCooldown)
        {
            canAttack = true;
        }
    }

    protected abstract void Attack(PlayerController player);
    
    public void TakeDamage(float damage){
        Instantiate(bloodEffect,transform.position, Quaternion.identity);
        Debug.Log(">>> Enemy life before: " + health);
        health -= damage;
        Debug.Log(">>> Enemy life after: " + health);
        Debug.Log("Damage taken "+damage);
    }
}
