using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingMedievalEnemy : BaseEnemy
{
    private void Update()
    {
        if (health <= 0)
        {
            Debug.Log(">>> Enemi life" + health);
            Destroy(gameObject);
            return;
        }

        VerifyCooldown();
        chase = Physics2D.OverlapCircle(transform.position, attackRange, playerLayer);

        if (player != null && chase && canAttack)
        {
            Chase();
        }
        else
        {
            ReturnToStartingPoint();
        }

        Flip();
    }

    protected override void Attack(PlayerController player)
    {
        player.Hit(1);
        canAttack = false;
        timeSinceAttack = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Attack(collision.GetComponent<PlayerController>());
        }
    }
}
