using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingMedievalEnemy : BaseEnemy
{
    public float jumpForce = 400f;

    private void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
            return;
        }

        VerifyCooldown();
        chase = Physics2D.OverlapCircle(transform.position, attackRange, playerLayer);

        if (player != null && chase && canAttack)
        {
            Chase();
        }
        else
        {
            ReturnToStartingPoint();
        }

        Flip();
    }

    protected override void Attack(PlayerController player)
    {
        player.Hit(1);
        canAttack = false;
        timeSinceAttack = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Attack(collision.GetComponent<PlayerController>());
        }
    }

    protected override void Chase()
    {
        var target = new Vector2(player.transform.position.x * 50f, player.transform.position.y * jumpForce);
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        //transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }
}