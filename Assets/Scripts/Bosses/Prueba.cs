using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prueba : BaseEnemy
{
    private void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
            PlayerController playerTest = player.GetComponent<PlayerController>();
            playerTest.PersistState();
            SceneManager.LoadScene(3);
            return;
        }

        VerifyCooldown();
        chase = Physics2D.OverlapCircle(transform.position, attackRange, playerLayer);

        if (player != null && chase && canAttack)
        {
            Chase();
        }

        Flip();
    }

    protected override void Attack(PlayerController player)
    {
        player.Hit(1);
        canAttack = false;
        timeSinceAttack = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Attack(collision.GetComponent<PlayerController>());
        }
    }
}
