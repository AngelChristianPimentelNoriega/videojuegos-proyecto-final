using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController
{
    private static GameController instance;

    public int vida = 10;
    public int maxVida = 10;

    public string clase = "";

    private GameController() { }

    public static GameController Instance {
        get
        {
            if (instance == null)
                instance = new GameController();
            return instance;
        }
    }
}
