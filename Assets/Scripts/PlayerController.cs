using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

[Serializable]
public class PlayerState
{
    public int life;
    public int maxLife;
    public int speed;
    public int idPlayerClass;
}

public class PlayerController : MonoBehaviour
{
    public float jumpForce = 600f;

    public bool saltando = false;

    public bool isDead;

    public int life;

    public int maxLife;

    public int speed;

    private Rigidbody2D rb;

    private Animator animador;

    private SpriteRenderer sr;

    public IClass playerClass;

    private int idPlayerClass;

    public float damage;

    private float timeBtwAttack;

    public float startTimeBtwAttack;

    public Transform attackPos;

    public LayerMask whatIsEnemies;

    public float attackRange;

    public GameObject bloodEffect;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animador = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();

        string previousPlayeState = PlayerPrefs.GetString("player_state");

        if (previousPlayeState != "")
        {
            PlayerState state = JsonUtility.FromJson<PlayerState>(previousPlayeState);
            idPlayerClass = state.idPlayerClass;
            playerClass = ClassFactory.createClass(idPlayerClass);
            maxLife = state.maxLife;
            life = state.life;
            speed = state.speed;
            PlayerPrefs.SetString("player_state", "");
        }
        else
        {
            int selectedClass = UnityEngine.Random.Range(0, 3);
            idPlayerClass = selectedClass;
            playerClass = ClassFactory.createClass(selectedClass);

            maxLife = playerClass.Health;
            life = playerClass.Health;
        }

        GameController gameController = GameController.Instance;
        gameController.vida = life;
        gameController.maxVida = maxLife;
        gameController.clase = playerClass.Name;
    }

    public void Hit(float damage)
    {
        Instantiate(bloodEffect,transform.position, Quaternion.identity);
        float probability = UnityEngine.Random.value;
        if (probability > 0 && probability < 0.5 && playerClass.Name == "Tank Class")
        {
            return;
        }
        life -= Mathf.FloorToInt(damage);
        GameController.Instance.vida -= Mathf.FloorToInt(damage);
        if (life <= 0)
        {
            isDead = true;
            SceneManager.LoadScene(2);
        }
    }

    public void IncreaseLife()
    {
        life += 1;
        GameController.Instance.vida += 1;
        if (life > maxLife)
        {
            life = maxLife;
        }
    }


    void PlayerAttack(){
        if(timeBtwAttack <= 0){
            if(Input.GetKey(KeyCode.Space)){
                animador.SetTrigger("attack");
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
                for(int i = 0; i < enemiesToDamage.Length; i++){
                    enemiesToDamage[i].GetComponent<BaseEnemy>().TakeDamage(damage);
                }
            }
            timeBtwAttack = startTimeBtwAttack;
        } else {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    void OnDrawGizmosSelected(){
        
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);

    }

    void Update()
    {
        if (isDead)
        {
            gameObject.SetActive(false);
            return;
        }

        PlayerAttack();

        float deltaX = Input.GetAxis("Horizontal") * (speed * playerClass.Speed) * Time.deltaTime;
        Vector2 movement = new(deltaX, rb.velocity.y);
        rb.velocity = movement;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            animador.SetBool("corriendo",true);
            sr.flipX = false;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            animador.SetBool("corriendo",true);
            sr.flipX = true;
        }

        if (!Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
        {
            animador.SetBool("corriendo",false);
        }
        

        if (Input.GetKeyDown(KeyCode.UpArrow) && !saltando)
        {
            saltando = true;
            rb.AddForce(new Vector2(0,jumpForce));
            animador.SetBool("saltando",true);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Suelo"))
        {
            saltando = false;
            animador.SetBool("saltando",false);
        }

        if (other.transform.CompareTag("Dano"))
        {
            Hit(1);
        }
    }

    public void PersistState(bool saveForNewGame = false)
    {
        PlayerState playerState = new();
        if (saveForNewGame)
        {
            playerState.life = playerClass.Health;
            playerState.maxLife = playerClass.Health;
        }
        else
        {
            playerState.life = life;
            playerState.maxLife = maxLife;
        }
        playerState.idPlayerClass = idPlayerClass;
        playerState.speed = speed;

        string json = JsonUtility.ToJson(playerState);
        PlayerPrefs.SetString("player_state", json);
    }
}
