using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public PlayerController player;
    public GameObject starterPoint;

    void Start()
    {
        Instantiate(player, starterPoint.transform);
    }
}
