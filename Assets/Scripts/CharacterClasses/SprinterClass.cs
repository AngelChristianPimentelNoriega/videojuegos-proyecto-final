using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprinterClass : IClass
{
    public int Health => 10;

    public float Defense => 0.8f;

    public float Strength => 0.8f;

    public float Speed => 2f;

    public string Name => "Sprinter Class";
}
