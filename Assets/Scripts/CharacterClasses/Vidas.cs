using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vidas : MonoBehaviour
{
    private GameController gameController;
    public int vida ; 
    public int maxVida;
    public Text jclase;
    public Image[] corazones;
    public Sprite corazonVida;
    public Sprite corazonVacio;

    private void Start()
    {
        gameController = GameController.Instance;
        jclase.text = gameController.clase;
    }

    // Update is called once per frame
    void Update(){
        vida = gameController.vida; 
        maxVida = gameController.maxVida;
        jclase.text = gameController.clase;

        if(vida > maxVida){
            vida = maxVida;
        }
        
        for(int i = 0; i < corazones.Length; i++){
            if(i < vida){
                corazones[i].sprite = corazonVida;
            }
            else{
                corazones[i].sprite = corazonVacio;
            }

            if(i < maxVida){
                corazones[i].enabled = true;
            }
            else{
                corazones[i].enabled = false;
            }
        }
        
    }
}
