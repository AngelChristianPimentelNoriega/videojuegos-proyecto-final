using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerserkerClass : IClass
{
    public int Health => 10;

    public float Defense => 1f;

    public float Strength => 3f;

    public float Speed => 1f;

    public string Name => "Berserker Class";
}
