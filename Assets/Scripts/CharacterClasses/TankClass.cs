using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankClass : IClass
{
    public int Health => 10;

    public float Defense => 5f;

    public float Strength => 1f;

    public float Speed => 0.5f;

    public string Name => "Tank Class";
}
