using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClass
{
    int Health { get; }
    float Defense { get; }
    float Strength { get; }
    float Speed { get; }
    string Name { get; }
}