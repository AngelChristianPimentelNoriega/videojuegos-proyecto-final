using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassFactory
{
    public static IClass createClass(int selectedClass)
    {
        switch (selectedClass)
        {
            case 0:
                return new TankClass();
            case 1:
                return new BerserkerClass();
            case 2:
                return new SprinterClass();
            default:
                throw new KeyNotFoundException("Clase no encontrada");
        }
    }
}
