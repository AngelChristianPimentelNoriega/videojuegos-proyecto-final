using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GameOverScreen : MonoBehaviour
{
    public AudioSource clip;

    public void RestartGame() {
        Debug.Log("Restart");
        SceneManager.LoadScene(1);
    
    }
    public void RestartSoundBotton(){
        clip.Play();
    }

}
