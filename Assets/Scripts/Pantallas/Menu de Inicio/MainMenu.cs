using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour
{
    public AudioSource clip;

    public void PlayGame() {
        Debug.Log("Play");
        SceneManager.LoadScene(1);

    }

    public void QuitGame() {
        
        Debug.Log("Quit!");
        Application.Quit();
    }

    public void PlaySoundBotton(){
        clip.Play();
    }
}

