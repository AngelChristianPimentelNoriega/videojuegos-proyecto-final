using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] maps;
    public Vidas vida;

    void Start()
    {
        GameObject map = maps[Random.Range(0,maps.Length)];
        Instantiate(map);
    }
}
