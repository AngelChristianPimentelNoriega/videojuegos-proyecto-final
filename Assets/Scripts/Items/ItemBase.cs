using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemBase : MonoBehaviour
{
    public float probabilityOfSpawn;

    protected abstract void ApplyEffect(PlayerController player);

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null && collision.CompareTag("Player"))
        {
            Debug.Log(">>> Add Effect");
            var player = collision.GetComponent<PlayerController>();
            ApplyEffect(player);
            Destroy(gameObject);
        }
    }
}
