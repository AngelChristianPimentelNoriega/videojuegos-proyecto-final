using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swordplus : ItemBase
{
    protected override void ApplyEffect(PlayerController player)
    {
        Debug.Log(">>> Player state: " + player.playerClass.Strength);
        player.damage += 1;
    }
}