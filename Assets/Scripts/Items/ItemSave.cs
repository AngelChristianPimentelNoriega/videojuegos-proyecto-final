using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSave : ItemBase
{
    protected override void ApplyEffect(PlayerController player)
    {
        player.PersistState(true);
    }
}
